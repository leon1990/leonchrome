package com.leon.chrome.utils;

public interface WebViewJavaScriptFunction {

	void onJsFunctionCalled(String tag);
}
